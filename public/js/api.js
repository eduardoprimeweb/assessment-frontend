let endpoint = 'http://localhost:8888/api/V1/categories/'
let breadcrumb =  $('.breadcrumb #ref');
let text_title_products = $('#title-products');

$.get(endpoint+ 'list', function(data, status){
    let menu = '';
    for(let x in data.items){
      menu +=`
        <li><a class="content-menu-js" data-id-item="${data.items[x].id}" href="javascript:void(0)">${data.items[x].name}</a></li>
       `
    }
    $('#menu-site li').eq(0).after(menu);
  });


  $(document).on('click', '.content-menu-js', function(){
    breadcrumb.text($(this).text());
    text_title_products.text($(this).text());
    let item_id = $(this).data('id-item');
    $.get(endpoint+ item_id, function(data, status){
      let grid_produto = '';
      for(let x in data.items){
        grid_produto +=`
          <div class="col-6 col-md-4 col-lg-3">
            <div class="box-item-produto">
                <div class="box-img">
                    <img src="${data.items[x].image}" alt="">
                </div>
                <div class="content">
                    <h3>${data.items[x].name}</h3>
                    <h4>R$ ${data.items[x].price}</h4>
                    <a href="" title="Comprar" class="comprar">Comprar</a>
                </div>
            </div>
          </div>
         `
      }
      $('#content').html(grid_produto);
    });

  });